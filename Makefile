build: vm
	echo OK

clean: deletevm
	rm -fr cache

download:
	mkdir -p cache
	wget --continue "https://cloud-images.ubuntu.com/releases/bionic/release/ubuntu-18.04-server-cloudimg-amd64.vmdk" -O cache/ubuntu.vmdk

iso:
	mkdir -p target/ubuntu
	sed "s/^instance-id.*/instance-id: $(shell uuid)/" meta-data.yaml > target/meta-data.yaml
	cloud-localds target/ubuntu/cloud-init.iso --network-config network-config.yaml user-data.yaml target/meta-data.yaml

deletevm:
	-vboxmanage controlvm ubuntu poweroff
	-vboxmanage unregistervm --delete ubuntu
	rm -fr target

vm: download deletevm iso
	vboxmanage createvm --name ubuntu --basefolder $(PWD)/target
	vboxmanage registervm $(PWD)/target/ubuntu/ubuntu.vbox
	cp cache/ubuntu.vmdk target/ubuntu/sda.vmdk
	vboxmanage modifyvm ubuntu --cpus 2 --memory 4096 \
		--nic1 nat --nicproperty1 name=eth0 \
		--nic2 hostonly --nicproperty1 name=eth1 \
		--hostonlyadapter2 vboxnet0 \
		--uart1 0x3f8 4 --uartmode1 file /tmp/serial.log \
		--audio none
	vboxmanage storagectl ubuntu --add sata --name SATA --hostiocache on --portcount 2
	vboxmanage storageattach ubuntu --storagectl SATA --port 0 --type hdd --medium target/ubuntu/sda.vmdk
	vboxmanage storageattach ubuntu --storagectl SATA --port 1 --type dvddrive --medium target/ubuntu/cloud-init.iso

export: vm
	rm -fr target/ubuntu.ovf target/ubuntu-*.vmdk
	vboxmanage export ubuntu --output target/ubuntu.ovf --ovf10
