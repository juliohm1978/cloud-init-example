# cloud-init Exemplificado com VirtualBox

`cloud-init` é um padrão criado para facilitar a inicialização de sistemas operacionais no ambiente *cloud*. Com ele, é possível configurar o sistema operacional antes mesmo dele ser iniciado.

> Documentação oficial: https://cloudinit.readthedocs.io/en/latest/

A configuração é *declarada* em arquivos *Yaml* ou *scripts shell* que são injetados na primeira vez que o sistema operacional é inciado. Algumas configurações são aplicadas somente no primeiro *boot* e outras podem ser re-aplicadas sempre que o sistema for reiniciado.

De forma resumida, o cloud-init executa vários módulos nas várias etapas de inicialização do sistema operacional, podendo configurar desde as interfaces de rede, nome do servidor, discos, partições, até a criação de usuários e arquivos diversos no filesystem. Como todas essas informações são pré-determinadas antes mesmo do SO ser iniciado, o provisionamento de novas instâncias se torna um processo rápido, previsível que pode ser facilmente repetido e automatizado.

## O que temos neste repositório

O conteúdo deste repositório demonstra um exemplo prático de como usar o cloud-init localmente, em sua estação de trabalho. O exemplo usa VirtualBox para criar e gerenciar as máquinas virtuais e programas do pacote `cloud-init-utils`.

O exemplo aqui demontrado foi criado para Linux, na distribuição Ubuntu.

Antes de iniciar, instale as dependências.

```shell
sudo apt install cloud-image-utils make
```

Este exemplo também presume que você já tem o [VirtualBox](https://www.virtualbox.org/) instalado e devidamente configurado com uma rede do tipo *[Host-Only-Network](https://www.virtualbox.org/manual/ch06.html#network_hostonly)* de nome **`vboxnet0`**.

## Como executar o exemplo

Faça uma cópia local do repositório:

```shell
git clone https://gitlab.com/juliohm1978/cloud-init-example.git
```

Para construir a máquina virtual:

```shell
cd cloud-init-example
make
```

As seguintes etapas serão realizadas automaticamente:

* Baixar uma imagem do ubuntu-cloud (atualmente a versão bionic)
* Criar uma imagem ISO com as configurações do cloud-init
* Criar uma VM (nome: ubuntu)
* Configurar a VM para usar a imagem cloud
* Anexar a ISO na VM

A partir deste ponto, a VM está pronta para ser iniciada.

Antes de fazer o primeiro *boot*, a VM pode ser exportada para ser usada em outro ambiente.

```shell
make export
```

A exportação é feita no formato OVF 1.0, compatível com outras plataformas de virtualização (ex: VMWare ou vSphere).

> NOTA: Todo conteúdo criado será colocado no diretório `target`. O download da imagem ubuntu original acontecerá apenas uma vez e será mantida no diretório `cache`.

Para limpar sua área de trabalho e apagar todo conteúdo criado:

```shell
make clean
```

## Usando a VM criada

Ao ligar a VM, aguarde o primeiro boot ser feito. Quanto cloud-init terminar, o usuário `user`, senha `user`, com permissões para `sudo`, terá sido criado. Você pode usá-lo para entrar no SO e brincar a vontade.

Este exemplo também tenta configurar as duas placas de rede da VM:

* `enp0s3` Com acesso NAT para sua Internet
* `enp0s8` Com acesso Host-Only-Network

Ao descobrir o IP da interface `enp0s8`, você pode fazer o acesso remoto à VM por ssh.

Você pode repetir o processo todo usando configurações diferentes. Desligue a VM, edite o arquivo `user-data.yaml` e refaça o procedimento do zero.

```shell
make vm
```

Para limpar toda a área de trabalho, incluindo o cache de imagens baixadas:

```shell
make clean
```

## Console Serial

Devido a um problema de configuração na imagem cloud do ubuntu, o SO não consegue iniciar corretamente sem a presença de uma porta serial para o console principal.

https://bugs.launchpad.net/cloud-images/+bug/1573095

Por isso, a VM criada possui uma porta serial COM1 configurada para enviar seu conteúdo ao arquivo `/tmp/serial.log`. A única vantagem é que você pode conferir o resultado do boot da máquina neste arquivo. Também é possível ver mensagens do cloud-init enquanto ele executa algumas tarefas.

Antes de iniciar a VM, faça um `tail` em outro terminal para assitir o boot do SO.

```shell
tail -F /tmp/serial.log
```
